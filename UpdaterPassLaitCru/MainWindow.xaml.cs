﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;

namespace UpdaterPassLaitCru
{
    /// <inheritdoc cref="Window" />
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        #region Fields

        private bool isIndeterminate;
        private bool isRunning;

        #endregion Fields

        #region Constructors

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Events

        #region Properties

        public bool IsIndeterminate
        {
            get => isIndeterminate;
            set
            {
                isIndeterminate = value;
                OnPropertyChanged(nameof(IsIndeterminate));
            }
        }

        #endregion Properties

        #region Methods

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (isRunning) return;

            Close();
        }

        private async void UpdateButton_OnClick(object sender, RoutedEventArgs e)
        {
            CancelButton.IsEnabled = false;
            PrgBarTask.IsIndeterminate = true;
            LogLabel.Content = "Mise à jour";
            isRunning = true;
            // Progress Events
            var progress = new Progress<ProgressReport>(CallBackReportProgress);


            var process = new UpdaterProcess(progress);


            isRunning = false;
            UpdateButton.IsEnabled = false;

            var log = await process.Update();
            //await Task.Delay(5000);

            //LogLabel.Content = "Mise à jour terminé";
            PrgBarTask.IsIndeterminate = false;
            CancelButton.IsEnabled = true;
        }

        /// <summary>
        ///  report progress Task From PlcSynchro class
        /// </summary>
        /// <param name="report"></param>
        private void CallBackReportProgress(ProgressReport report)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(report.Message))
                    LogLabel.Content = report.Message;

                if (PrgBarTask.Value < report.Percentage)
                    PrgBarTask.Value = report.Percentage;

#if DEBUG
                Debug.WriteLine($"Message: {report.Message}\r\n" +
                                $"Pourcentage: {report.Percentage}\r\n");
#endif
            }
            catch (Exception ex)
            {
                ex.LogException("MainWindows : CallBackReportProgress ");
                
            }
        }



        #endregion Methods

    }
}
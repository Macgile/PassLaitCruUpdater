﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

//using IWshRuntimeLibrary;

// ReSharper disable InvertIf

// ReSharper disable UnusedAutoPropertyAccessor.Global

// ReSharper disable MemberCanBePrivate.Global

namespace UpdaterPassLaitCru
{
    public class UpdaterProcess
    {
        #region Properties

        private readonly string accessFile;
        private readonly string appVersion;
        private readonly string desktopPath;
        private readonly bool isVersionTech;
        private readonly IProgress<ProgressReport> progress;
        private bool isFirstInstall;
        private bool updateModels;
        private bool updatePassLaitCru;
        private bool updatePassLaitCruTech;
        private bool updateSynchro;
        public FtpClient FtpClient { get; set; }
        public string FtpFolder { get; set; }
        public string FtpPassword { get; set; }
        public string FtpServer { get; set; }
        public string FtpUserName { get; set; }
        public string LocalFolder { get; set; }
        public VersionList LocalVersion { get; set; }
        public string ManifestName { get; set; }
        public VersionList RemoteVersion { get; set; }
        public string UpdateFolder { get; set; }

        #endregion Properties

        #region Constructors

        public UpdaterProcess(IProgress<ProgressReport> progressParam)
        {
            progress = progressParam;

            // Ftp Infos
            FtpServer = ConfigurationManager.AppSettings["FTPServer"];
            FtpUserName = ConfigurationManager.AppSettings["FTPUserName"];
            FtpPassword = ConfigurationManager.AppSettings["FTPPassword"];
            FtpFolder = ConfigurationManager.AppSettings["FTPFolder"];

            // init FtpClient
            FtpClient = new FtpClient(FtpServer, FtpUserName, FtpPassword);

            // File, Path and Folder
            ManifestName = "manifest.xml";
            LocalFolder = AppDomain.CurrentDomain.BaseDirectory;
            UpdateFolder = Path.Combine(LocalFolder, "update");

            desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            // Create ShortCut of this Application on the deskTop
            CreateShortcut("Mise à jour PassLaitCru", Process.GetCurrentProcess().MainModule.FileName);

            // default application name
            accessFile = "PassLaitCru.accdb";

            // get version of pass12, for controler or tech specialist
            if (Directory.GetFiles(LocalFolder, "PassLaitCru.accdb").Any())
            {
                isVersionTech = false;
                accessFile = "PassLaitCru.accdb";
            }
            else if (Directory.GetFiles(LocalFolder, "*Tech*.accdb").Any())
            {
                isVersionTech = true;
                accessFile = "PassLaitCruTech.accdb";
            }

            // get version off Synchro, 32/64 bits
            appVersion = Regex.Match(LocalFolder, @"\d+").Value;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Main Process task
        /// </summary>
        public async Task<string> Update()
        {
            // get manifest
            await Task.Run(() => GetManifest());

            if (CompareVersion())
            {
                // get file to update and install

                await Task.Run(() => UpdateFiles());

                // deleting all file
                Log("Nettoyage des dossiers");
                DeletingFiles();

                Log($"Mise à jour terminé, nombre d'erreur: {ExceptionExtensions.ErrorCount}");
                Debug.WriteLine("End of update");

                return "Terminé";
            }

            Log("Aucune mise à jour disponible.");
            Debug.WriteLine("No update available");

            return "log";
        }

        /// <summary>
        /// Extract file into specified directory
        /// </summary>
        /// <param name="zipPath"></param>
        /// <param name="extractPath"></param>
        private static bool ExtractFile(string zipPath, string extractPath)
        {
            try
            {
                ZipFile.ExtractToDirectory(zipPath, extractPath);
                return true;
            }
            catch (Exception ex)
            {
                ex.LogException($"Error ExtractFile: {zipPath}");
            }
            return false;
        }

        /// <summary>
        /// Compare the two versions between local and remote file
        /// </summary>
        /// <returns></returns>
        private bool CompareVersion()
        {
            // no Compare possible
            if (LocalVersion == null || RemoteVersion == null)
            {
                // considered to be the first installation
                isFirstInstall = true;
                return true;
            }

            var query = (from l in LocalVersion?.VersionInfos
                         join r in RemoteVersion.VersionInfos on l.Name equals r.Name
                         where string.Compare(r.Version, l.Version, StringComparison.Ordinal) > 0
                         group new { r, l } by new { r.Name, r.Version }
                into g
                         select new
                         {
                             g.Key.Name,
                             g.Key.Version
                         }).ToList();

            updateSynchro = query.FirstOrDefault(s => s.Name == "Synchro") != null;
            updatePassLaitCru = query.FirstOrDefault(s => s.Name == "PassLaitCru") != null;
            updateModels = query.FirstOrDefault(s => s.Name == "Modeles") != null;
            updatePassLaitCruTech = query.FirstOrDefault(s => s.Name == "PassLaitCruTech") != null;

            // return if any update is available
            if (isVersionTech)
                return updateSynchro || updatePassLaitCruTech || updateModels;
            return updateSynchro || updatePassLaitCru || updateModels;
        }

        /// <summary>
        /// Download single file in folder
        /// </summary>
        /// <param name="remoteFolder"></param>
        /// <param name="localFolder"></param>
        /// <param name="remoteFilename"></param>
        /// <param name="localFileName"></param>
        private bool DownloadSingleFiles(string remoteFolder, string localFolder, string remoteFilename,
            string localFileName = "")
        {
            // if localFileName is empty, the remote and local file have a same name
            if (string.IsNullOrEmpty(localFileName))
                localFileName = remoteFilename;

            return FtpClient.Download($"{remoteFolder}/{remoteFilename}", $@"{localFolder}\{localFileName}");
        }

        /// <summary>
        /// Create the necessary directories if it does not exist.
        /// And get manifest file of FTP server  and compare it to local manifest if exist
        /// </summary>
        private void GetManifest()
        {
            try
            {
                if (!Directory.Exists(UpdateFolder))
                {
                    // when this directory doesn't exist, this is the first install.
                    isFirstInstall = true;
                    Directory.CreateDirectory(UpdateFolder);
                }

                Log("Vérification des mise à jours disponible.");
                Debug.WriteLine("Download remote manifest file");

                // if remote manifest file doesn't exist,  exit
                if (!DownloadSingleFiles(FtpFolder, UpdateFolder, "version.xml", "manifest.xml")) return;

                // Read Manifest Files
                LocalVersion = XmlReaderFile.GetManifest($"{UpdateFolder}/version.xml");
                RemoteVersion = XmlReaderFile.GetManifest($"{UpdateFolder}/manifest.xml");
            }
            catch (IOException ex)
            {
                ex.LogException("GetManifest, Erreur de création des dossier");
            }
            catch (Exception ex)
            {
                ex.LogException("GetManifest");
            }
        }

        /// <summary>
        /// Update PassLaitCru
        /// </summary>
        private void UpdateAccessFile(string fileName)
        {
            Debug.WriteLine($"Update {fileName} file");
            if (DownloadSingleFiles(FtpFolder, LocalFolder, accessFile))
            {
                CreateShortcut(fileName, $@"{LocalFolder}\{accessFile}");
            }
        }

        /// <summary>
        /// Get files from FTp
        /// </summary>
        private void UpdateFiles()
        {
            var versionInfos = RemoteVersion.VersionInfos;

            try
            {
                // Update Only PassLaitCru
                if ((updatePassLaitCru || isFirstInstall) && !isVersionTech)
                {
                    var version = versionInfos.First(v => v.Name == "PassLaitCru").Version;
                    Log($"Téléchargement et installation de PassLaitCru v{version}");
                    UpdateAccessFile("PassLaitCru");
                }

                // Update Only PassLaitCruTech
                if ((updatePassLaitCruTech || isFirstInstall) && isVersionTech)
                {
                    var version = versionInfos.First(v => v.Name == "PassLaitCruTech").Version;
                    Log($"Téléchargement et installation de PassLaitCru Tech v{version}");
                    UpdateAccessFile("PassLaitCruTech");
                }

                // Update Only Synchro
                if (updateSynchro || isFirstInstall)
                {
                    var version = versionInfos.First(v => v.Name == "Synchro").Version;
                    Log($"Téléchargement et installation de la Synchronisation v{version}");
                    UpdateZipFolder(FtpFolder, "Synchro", appVersion);
                }
                // Update Only Modeles
                if (updateModels || isFirstInstall)
                {
                    var version = versionInfos.First(v => v.Name == "Modeles").Version;
                    Log($"Téléchargement et installation des modeles v{version}");
                    UpdateZipFolder(FtpFolder, "Modeles");
                }
            }
            catch (Exception ex)
            {
                ex.LogException($"Error UpdateFiles: Is First Install: {isFirstInstall}\r\n" +
                                $"Update PassLaitCru: {updatePassLaitCru}\r\n" +
                                $"Update Synchro: {updateSynchro}\r\n" +
                                $"Update Models: {updateModels}");
            }
            finally
            {
                // move, and rename the manifest.xml file as version.xml in the update folder
                File.Copy($@"{UpdateFolder}\manifest.xml", $@"{UpdateFolder}\version.xml", true);
            }
        }

        /// <summary>
        /// Update Synchro files
        /// </summary>
        /// <returns></returns>
        private void UpdateZipFolder(string remoteFolder, string folderName, string version = "")
        {
            Debug.WriteLine($"Update {folderName}");

            if (DownloadSingleFiles(remoteFolder, UpdateFolder, $"{folderName}{version}.zip"))
            {
                if (Directory.Exists(folderName))
                    Directory.Move(folderName, $"{folderName}.back");

                if (!ExtractFile($@"{UpdateFolder}\{folderName}{version}.zip", $@"{LocalFolder}\{folderName}"))
                {
                    // rollback rename directory
                    if (Directory.Exists($"{folderName}.back"))
                        Directory.Move($"{folderName}.back", folderName);
                }
                else
                {
                    if (Directory.Exists($"{folderName}.back"))
                        Directory.Delete($"{folderName}.back", true);
                }
            }
        }

        /// <summary>
        /// Create Shortcup in desktop
        /// </summary>
        private void CreateShortcut(string fileName, string appPath)
        {
            try
            {
                // delete old ShortCut
                DeleteShortCut(fileName);

                var shortCutPath = $@"{desktopPath}\{fileName}.lnk";
                var shell = new IWshRuntimeLibrary.WshShell();
                var shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortCutPath);
                shortcut.Description = "Raccourci vers PassLaitCru - Phareway";
                shortcut.TargetPath = appPath;
                shortcut.Save();
            }
            catch (Exception ex)
            {
                ex.LogException("Create ShortCut");
            }
        }

        /// <summary>
        /// Delete ShortCut in desktop
        /// </summary>
        /// <param name="fileName"></param>
        private void DeleteShortCut(string fileName)
        {
            try
            {
                foreach (var file in Directory.GetFiles(desktopPath, $"{fileName}*.lnk").ToList())
                {
                    if (File.Exists(file))
                        File.Delete(file);
                }
            }
            catch (Exception ex)
            {
                ex.LogException("DeleteShortCut ");
            }
        }

        /// <summary>
        /// Removes all files from the update directory
        /// </summary>
        private void DeletingFiles()
        {
            // deleting files and directory
            var fileName = string.Empty;
            var dirName = string.Empty;

            try
            {
                var di = new DirectoryInfo(UpdateFolder);

                foreach (var file in di.GetFiles().Where(f => f.Name != "version.xml"))
                {
                    fileName = file.Name;
                    file.Delete();
                }
                foreach (var dir in di.GetDirectories())
                {
                    dirName = dir.Name;
                    dir.Delete(true);
                }

                Debug.WriteLine("Success of Deleting files");
            }
            catch (Exception ex)
            {
                ex.LogException($"Error Deleting the file: {fileName}, Directory: {dirName}");
            }
        }

        /// <summary>
        /// Report message to Log in UI
        /// </summary>
        /// <param name="msg"></param>
        public void Log(string msg = "")
        {
            var report = new ProgressReport
            {
                Message = msg,
                Total = 0,
                Percentage = 0
            };
            progress?.Report(report);
        }

        #endregion Methods
    }
}
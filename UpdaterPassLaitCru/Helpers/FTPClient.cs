﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

// ReSharper disable CheckNamespace

namespace UpdaterPassLaitCru
{
    public class FtpClient
    {
        #region Fields

        // The hostname or IP address of the FTP server.
        private readonly string remoteHost;

        // Password for the remote user
        private readonly string remotePass;

        // The remote username
        private readonly string remoteUser;

        #endregion Fields

        #region Constructors

        public FtpClient()
        {
        }

        public FtpClient(string remoteHost, string remoteUser, string remotePass)
        {
            this.remoteHost = remoteHost;
            this.remoteUser = remoteUser;
            this.remotePass = remotePass;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Remove a file from the server.
        /// </summary>
        /// <param name="filename">filename and path to the file, e.g. public_html/test.zip</param>
        public void DeleteFileFromServer(string filename)
        {
            var request = (FtpWebRequest)WebRequest.Create(remoteHost + filename);
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            request.Credentials = new NetworkCredential(remoteUser, remotePass);
            var response = (FtpWebResponse)request.GetResponse();
            response.Close();
        }

        /// <summary>
        /// Get a list of files and folders on the FTP server
        /// </summary>
        /// <returns></returns>
        public List<string> DirectoryListing()
        {
            return DirectoryListing(string.Empty);
        }

        /// <summary>
        /// List files and folders in a given folder on the server
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="mask"></param>
        /// <returns></returns>
        public List<string> DirectoryListing(string folder, string mask = "")
        {
            var request = (FtpWebRequest)WebRequest.Create($"{remoteHost}/{folder}");
#if DEBUG
            Debug.WriteLine(request.RequestUri.ToString());
#endif

            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(remoteUser, remotePass);
            request.UseBinary = true;
            request.Proxy = null;
            request.KeepAlive = false;
            request.UsePassive = true;

            var response = (FtpWebResponse)request.GetResponse();
            var responseStream = response.GetResponseStream();

            if (responseStream == null) return null;

            var reader = new StreamReader(responseStream);

            var result = new List<string>();

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();

                if (line != null && (line.Trim() == "." || line.Trim() == ".."))
                    continue;

                if (mask != string.Empty)
                {
                    if (line != null && Regex.IsMatch(line, mask))
                    {
                        result.Add(line);
                    }
                }
                else
                {
                    result.Add(line);
                }
            }

            reader.Close();
            response.Close();
            return result;
        }

        /// <summary>
        /// Download a file from the FTP server to the destination
        /// </summary>
        /// <param name="filename">filename and path to the file, e.g. public_html/test.zip</param>
        /// <param name="destination">The location to save the file, e.g. c:test.zip</param>
        public bool Download(string filename, string destination)
        {
            try
            {
                // replace space in file name
                filename = filename.Replace(" ", "%20");
                var request = (FtpWebRequest)WebRequest.Create($"{remoteHost}/{filename}");
#if DEBUG
                Debug.WriteLine(request.RequestUri.ToString());
#endif
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(remoteUser, remotePass);
                request.UseBinary = true;
                request.Proxy = null;
                request.KeepAlive = false;
                request.UsePassive = true;

                var response = (FtpWebResponse)request.GetResponse();

                response.GetResponseStream();

                var buffer = new byte[2048]; var reader = request.GetResponse().GetResponseStream();

                using (var fileStream = new FileStream(destination, FileMode.Create))
                {
                    while (true)
                    {
                        if (reader == null) continue;

                        var bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                            break;
                        fileStream.Write(buffer, 0, bytesRead);
                    }
                }
                response.Close();
                return true;
            }
            catch (WebException ex)
            {
                ex.LogException("FTP  Download");
            }

            return false;
        }

        /// <summary>
        /// Upload a file to the server
        /// </summary>
        /// <param name="source">Full path to the source file e.g. c:test.zip</param>
        /// <param name="destination">destination folder and filename e.g. public_html/test.zip</param>
        public void UploadFile(string source, string destination)
        {
            var request = (FtpWebRequest)WebRequest.Create(remoteHost + destination);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(remoteUser, remotePass);

            var sourceStream = new StreamReader(source);
            var fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());

            request.ContentLength = fileContents.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);

            var response = (FtpWebResponse)request.GetResponse();

            response.Close();
            requestStream.Close();
            sourceStream.Close();
        }

        #endregion Methods
    }
}
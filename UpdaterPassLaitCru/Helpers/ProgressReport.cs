﻿// ReSharper disable CheckNamespace

namespace UpdaterPassLaitCru
{
    /// <summary>
    ///  Class For different report progress type
    /// </summary>
    public class ProgressReport
    {
        //current progress

        public int Percentage { get; set; }

        public int Total { get; set; }

        public bool State { get; set; }

        public string Message { get; set; }
    }
}

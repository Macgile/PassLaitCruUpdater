﻿using System;
using System.Diagnostics;
using System.Globalization;

// ReSharper disable CheckNamespace

namespace UpdaterPassLaitCru
{
    /// <summary>
    /// Exception Extension
    /// </summary>
    public static class ExceptionExtensions
    {
        #region Properties

        public static int ErrorCount { get; set; }

        #endregion Properties

        #region Methods

        private static Exception GetOriginalException(this Exception ex)
        {
            return ex.InnerException == null ? ex : ex.InnerException.GetOriginalException();
        }

        private static int LineNumber(this Exception ex)
        {
            var lineNumber          = 0;
            const string lineSearch = ":line ";
            var index               = ex.StackTrace.LastIndexOf(lineSearch, StringComparison.Ordinal);

            if (index               == -1) return lineNumber;

            var lineNumberText      = ex.StackTrace.Substring(index + lineSearch.Length);
            if (int.TryParse(lineNumberText, out lineNumber))
            {
            }
            return lineNumber;
        }

        public static void LogException(this Exception ex, string origin = "")
        {
            var innerEx = ex.GetOriginalException().Message;
            origin      = string.IsNullOrEmpty(origin) ? "Unknow" : origin;

            var line    = ex.LineNumber();

            var inner   = innerEx.Equals(ex.Message, StringComparison.OrdinalIgnoreCase) ? "no inner exception" : innerEx;

            var message = $"\r\n" +
                          $"{"Origin of Exception",-30}: {origin}\r\n" +
                          $"{"Message",-30}: {ex.Message}\r\n" +
                          $"{"Inner Exception",-30}: {inner}\r\n" +
                          $"{"Line",-30}: {line}";
#if DEBUG
            Debug.WriteLine(message);
#endif

            // Log message into log file
            Logger.LogFile(message);

            ErrorCount++;
        }

        #endregion Methods
    }

    public static class StringExtension
    {
        #region Methods

        // http://www.extensionmethod.net/csharp/string
        /// <summary>
        /// Return string to Title Case (all first word character in upperCase)
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ToProperCase(this string text)
        {
            var cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            var textInfo = cultureInfo.TextInfo;
            return textInfo.ToTitleCase(text);
        }

        public static string ToTitleCase(this string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        #endregion Methods
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace UpdaterPassLaitCru
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            // Lots of other important stuff here... 
            EncryptConfigSection("UpdaterPassLaitCru.Properties.Settings");
            base.OnStartup(e);
        }

        private void EncryptConfigSection(string sectionKey)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var section = config.AppSettings;
            if (section == null) return;
            if (section.SectionInformation.IsProtected) return;
            if (section.ElementInformation.IsLocked) return;

            section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            section.SectionInformation.ForceSave = true;
            config.Save(ConfigurationSaveMode.Full);
        }
    }
}
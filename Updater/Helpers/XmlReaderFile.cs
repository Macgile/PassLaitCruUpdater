﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Schema;
using System.Xml.Serialization;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace

// https://stackoverflow.com/questions/10518372/how-to-deserialize-xml-to-object

namespace Updater
{
    public static class XmlReaderFile
    {
        #region Properties

        /// <summary>
        /// List of versions in manifest file
        /// </summary>
        public static VersionList VersionInfos { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Call deserialize method
        /// </summary>
        /// <param name="pathFile"></param>
        public static VersionList GetManifest(string pathFile)
        {
            if (!File.Exists(pathFile)) return null;
            return File.Exists(pathFile) ? DeserializeXmlFileToObject<VersionList>(pathFile) : null;
        }

        /// <summary>
        /// Not Used
        /// Call deserialize method
        /// </summary>
        /// <param name="pathFile"></param>
        public static void ReadManifest(string pathFile)
        {
            VersionInfos = DeserializeXmlFileToObject<VersionList>(pathFile);
        }

        /// <summary>
        /// Deserialize xml file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlFilename"></param>
        /// <returns></returns>
        private static T DeserializeXmlFileToObject<T>(string xmlFilename)
        {
            var returnObject = default(T);
            if (string.IsNullOrEmpty(xmlFilename)) return default(T);

            try
            {
                using (var stream = new StreamReader(xmlFilename))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    returnObject = (T)serializer.Deserialize(stream);
                }
            }
            catch (XmlSchemaException ex)
            {
                ex.LogException("XmlReaderFile : XmlSchemaException");
            }
            catch (Exception ex)
            {
                ex.LogException("XmlReaderFile : DeserializeXmlFileToObject");
            }
            return returnObject;
        }

        #endregion Methods
    }

    /// <summary>
    /// Childs informations from manifest file
    /// </summary>
    public class VersionInfos
    {
        #region Properties

        [XmlElement("extension")] public string Extension;

        [XmlElement("name")] public string Name;

        [XmlElement("releasedate")] public string ReleaseDate;

        [XmlElement("version")] public string Version;

        #endregion Properties
    }

    /// <summary>
    /// Root of manifest file
    /// </summary>
    [XmlRoot("manifest")]
    public class VersionList
    {
        #region Properties

        [XmlElement("application")]
        public List<VersionInfos> VersionInfos { get; set; }

        #endregion Properties
    }
}
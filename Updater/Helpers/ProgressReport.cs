﻿// ReSharper disable CheckNamespace

namespace Updater
{
    /// <summary>
    ///  Class For different report progress type
    /// </summary>
    public class ProgressReport
    {
        //current progress
        public string Action { get; set; }

        public int Percentage { get; set; }

        public int Total { get; set; }

        public bool State { get; set; }

        public string Log { get; set; }
    }
}

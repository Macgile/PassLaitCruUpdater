﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;

// ReSharper disable UnusedAutoPropertyAccessor.Global

// ReSharper disable MemberCanBePrivate.Global

namespace Updater
{
    internal class UpdaterProcess
    {
        #region Properties

        private bool isFirstInstall;
        private bool updatePassLaitCru;
        private bool updateSynchro;
        private bool updateModels;
        private readonly string appVersion;
        public FtpClient FtpClient { get; set; }
        public string FtpFolder { get; set; }
        public string FtpPassword { get; set; }
        public string FtpServer { get; set; }
        public string FtpUserName { get; set; }
        public string LocalFolder { get; set; }
        public string ManifestName { get; set; }
        public string UpdateFolder { get; set; }
        public VersionList LocalVersion { get; set; }
        public VersionList RemoteVersion { get; set; }

        #endregion Properties

        #region Constructors

        public UpdaterProcess()
        {
            // Ftp Infos
            FtpServer = ConfigurationManager.AppSettings["FTPServer"];
            FtpUserName = ConfigurationManager.AppSettings["FTPUserName"];
            FtpPassword = ConfigurationManager.AppSettings["FTPPassword"];
            FtpFolder = ConfigurationManager.AppSettings["FTPFolder"];

            // init FtpClient
            FtpClient = new FtpClient(FtpServer, FtpUserName, FtpPassword);

            // File, Path and Folder
            ManifestName = "manifest.xml";
            LocalFolder = AppDomain.CurrentDomain.BaseDirectory;
            UpdateFolder = Path.Combine(LocalFolder, "update");
            //var dir = Directory.GetCurrentDirectory();

            // get version off Synchro, 32/64 bits
            // TODO: remove 32
            appVersion = Regex.Match(LocalFolder, @"\d+").Value;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Compare the two versions between local and remote file
        /// </summary>
        /// <returns></returns>
        private bool CompareVersion()
        {
            // no Compare possible
            if (LocalVersion == null || RemoteVersion == null)
            {
                // considered to be the first installation
                isFirstInstall = true;
                return true;
            }

            var query = (from l in LocalVersion?.VersionInfos
                join r in RemoteVersion.VersionInfos on l.Name equals r.Name
                where string.Compare(r.Version, l.Version, StringComparison.Ordinal) > 0
                group new {r, l} by new {r.Name, r.Version}
                into g
                select new
                {
                    g.Key.Name,
                    g.Key.Version
                }).ToList();

            updateSynchro = query.FirstOrDefault(s => s.Name == "Synchro") != null;
            updatePassLaitCru = query.FirstOrDefault(s => s.Name == "PassLaitCru") != null;
            updateModels = query.FirstOrDefault(s => s.Name == "Models") != null;

            // return if any update is available
            return updateSynchro || updatePassLaitCru || updateModels;
        }

        /// <summary>
        /// Create the necessary directories if it does not exist
        /// get manifest file of FTP server  and compare it to local manifest if exist
        /// </summary>
        private void GetManifest()
        {
            try
            {
                if (!Directory.Exists(UpdateFolder))
                {
                    // when this directory doesn't exist, this is the first install.
                    isFirstInstall = true;
                    Directory.CreateDirectory(UpdateFolder);
                }

                Debug.WriteLine("Download remote manifest file");

                // if remote manifest file doesn't exist,  exit
                if (!DownloadSingleFiles("PassLaitCru", UpdateFolder, "version.xml", "manifest.xml")) return;

                // Read Manifest Files
                LocalVersion = XmlReaderFile.GetManifest("update/version.xml");
                RemoteVersion = XmlReaderFile.GetManifest("update/manifest.xml");
            }
            catch (IOException ex)
            {
                ex.LogException("GetManifest, Erreur de création des dossier");
            }
            catch (Exception ex)
            {
                ex.LogException("GetManifest");
            }
        }

        /// <summary>
        /// Main Process task
        /// </summary>
        public void Update()
        {
            // get manifest
            GetManifest();

            if (CompareVersion())
            {
                // get file to update and install
                GetFtpFiles();

                // deleting all file
                DeletingFiles();

                Debug.WriteLine("End of update");

                return;
            }
            Debug.WriteLine("No update available");
        }

        /// <summary>
        /// Get files from FTp
        /// </summary>
        private void GetFtpFiles()
        {
            var statusPass = false;
            var statusSynchro = false;
            var statusModels = false;

            try
            {
                // Update Only PassLaitCru
                if (updatePassLaitCru || isFirstInstall)
                {
                    statusPass = UpdatePassLaitCru();
                }
                // Update Only Synchro
                if (updateSynchro || isFirstInstall)
                {
                    statusSynchro = UpdateZipFolder("PassLaitCru", "Synchro", appVersion);
                }
                // Update Only Synchro
                if (updateModels || isFirstInstall)
                {
                    statusModels = UpdateZipFolder("PassLaitCru", "Modeles");
                }
            }
            catch (Exception ex)
            {
                ex.LogException($"Error GetFtpFiles: Is First Install: {isFirstInstall}\r\n" +
                                $"Update PassLaitCru: {updatePassLaitCru}\r\n" +
                                $"Update Synchro: {updateSynchro}\r\n" +
                                $"Update Models: {statusModels}");
            }
            finally
            {
                // move manifest file to update folder
                File.Copy($@"{UpdateFolder}\manifest.xml", $@"{UpdateFolder}\version.xml", true);
                Debug.WriteLine(statusPass ? "Success Update PassLaitCru" : "");
                Debug.WriteLine(statusSynchro ? "Success Update Synchro" : "");
                Debug.WriteLine(statusModels ? "Success Update Modeles" : "");
            }
        }

        /// <summary>
        /// Update PassLaitCru
        /// </summary>
        private bool UpdatePassLaitCru()
        {
            Debug.WriteLine("Update PassLaitCru file");

            return DownloadSingleFiles("PassLaitCru", LocalFolder, "PassLaitCru.accdb");
        }

        /// <summary>
        /// Update Synchro files
        /// </summary>
        /// <returns></returns>
        private bool UpdateZipFolder(string remoteFolder, string folderName, string version = "")
        {
            Debug.WriteLine($"Update {folderName}");

            if (DownloadSingleFiles(remoteFolder, UpdateFolder, $"{folderName}{version}.zip"))
            {
                if (Directory.Exists(folderName))
                    Directory.Move(folderName, $"{folderName}.back");

                if (!ExtractFile($@"{UpdateFolder}\{folderName}{version}.zip", $@"{LocalFolder}\{folderName}"))
                {
                    // rollback rename directory
                    if (Directory.Exists($"{folderName}.back"))
                        Directory.Move($"{folderName}.back", folderName);
                }
                else
                {
                    if (Directory.Exists($"{folderName}.back"))
                        Directory.Delete($"{folderName}.back", true);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Download single file in folder
        /// </summary>
        /// <param name="remoteFolder"></param>
        /// <param name="localFolder"></param>
        /// <param name="remoteFilename"></param>
        /// <param name="localFileName"></param>
        private bool DownloadSingleFiles(string remoteFolder, string localFolder, string remoteFilename,
            string localFileName = "")
        {
            // if localFileName is empty, the remote and local file have a same name
            if (string.IsNullOrEmpty(localFileName))
                localFileName = remoteFilename;

            return FtpClient.Download($"{remoteFolder}/{remoteFilename}", $@"{localFolder}\{localFileName}");
        }

        /// <summary>
        /// Extract file into specified directory
        /// </summary>
        /// <param name="zipPath"></param>
        /// <param name="extractPath"></param>
        private static bool ExtractFile(string zipPath, string extractPath)
        {
            try
            {
                ZipFile.ExtractToDirectory(zipPath, extractPath);
                return true;
            }
            catch (Exception ex)
            {
                ex.LogException($"Error ExtractFile: {zipPath}");
            }
            return false;
        }

        /// <summary>
        /// Removes all files from the update directory
        /// </summary>
        private void DeletingFiles()
        {
            // deleting files and directory
            var fileName = string.Empty;
            var dirName = string.Empty;

            try
            {
                var di = new DirectoryInfo(UpdateFolder);

                foreach (var file in di.GetFiles().Where(f => f.Name != "version.xml"))
                {
                    fileName = file.Name;
                    file.Delete();
                }
                foreach (var dir in di.GetDirectories())
                {
                    dirName = dir.Name;
                    dir.Delete(true);
                }

                Debug.WriteLine("Success of Deleting files");
            }
            catch (Exception ex)
            {
                ex.LogException($"Error Deleting the file: {fileName}, Directory: {dirName}");
            }
        }

        #endregion Methods
    }
}
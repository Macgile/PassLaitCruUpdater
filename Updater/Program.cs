﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Updater
{
    class Program
    {
        static void Main(string[] args)
        {


//#if DEBUG
//            // Raise exception FirstChanceException when is not catched in try catch block
//            // comment the following block of code in release mode
//            AppDomain.CurrentDomain.FirstChanceException += (source, e) =>
//            {
//                Debug.WriteLine("FirstChanceException event raised in {0}: {1}",
//                    AppDomain.CurrentDomain.FriendlyName, e.Exception.Message);
//            };
//#endif
            var process = new UpdaterProcess();
            process.Update();

        }
    }
}
